import java.util.Arrays;
import java.util.Scanner;

public class matrizesAtv05 {

	public static void main(String[] args) {

		questao01();
		System.out.println();

	}

	/*
	 * 1) Leia uma matriz real 3x4 e depois exiba o elemento do canto superior
	 * esquerdo e o do canto inferior direito.
	 */
	public static void questao01() {

		Scanner sc = new Scanner(System.in);

		System.out.println("Informe a quantidade de linhas");
		int m = sc.nextInt();
		System.out.println("Informe a quantidade de colunas");
		int n = sc.nextInt();
		int[][] mat = new int[m][n];

		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat[i].length; j++) {
				System.out.println("Digite um n�mero");
				mat[i][j] = sc.nextInt();
			}
		}
		for (int k = 0; k < mat.length; k++) {
			System.out.println(Arrays.toString(mat[k]));
		}
		System.out.println("Digite um n�mero da matriz :");
		int x = sc.nextInt();

		for (int i = 0; i < mat.length; i++) { // mat.length encontra o valor das linhas
			for (int j = 0; j < mat[i].length; j++) { // mat[i].length encontra o n�mero de colunas
				if (mat[i][j] == x) {
					System.out.println("Posi��o " + i + "," + j + ":");
					if (j > 0) {
						System.out.println("A esquerda de " + x + " �:" + mat[i][j - 1]);
					}
					if (i > 0) {
						System.out.println("Acima de " + x + " �:" + mat[i - 1][j]);
					}
					if (j < mat[i].length - 1) {
						System.out.println("A direita de " + x + " �:" + mat[i][j + 1]);
					}
					if (i < mat.length - 1) {
						System.out.println("Abaixo de " + x + " �:" + mat[i + 1][j]);
					}
				}
			}
		}

		sc.close();
	}

	/*
	 * 2) Ler uma matriz 5x5 e gerar outra em que cada elemento � o cubo do elemento
	 * respectivo na matriz original. Imprima as duas matrizes.
	 */
	private static void questao02() {

		Scanner sc = new Scanner(System.in);

		System.out.println("Informe a quantidade de linhas");
		int m = sc.nextInt();
		System.out.println("Informe a quantidade de colunas");
		int n = sc.nextInt();
		int[][] mat = new int[m][n];
		int[][] mat2 = new int[m][n];

		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat[i].length; j++) {
				System.out.println("Digite um n�mero");
				mat[i][j] = sc.nextInt();
				mat2[i][j] = 3 * (mat[i][j]);
			}
		}

		for (int k = 0; k < mat.length; k++) {
			System.out.println(Arrays.toString(mat[k]));
		}
		for (int k = 0; k < mat.length; k++) {
			System.out.println(Arrays.toString(mat2[k]));
		}
	}

	/*
	 * 3) Leia uma matriz 3x3 e imprima a soma dos valores da diagonal principal.
	 */
	public static void questao03() {

		Scanner sc = new Scanner(System.in);
		System.out.println("Digite a ordem do vetor");
		int n = sc.nextInt();
		int[][] matriz = new int[n][n];

		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz.length; j++) {
				System.out.println("Digite os valores:");
				matriz[i][j] = sc.nextInt();
			}

		}
		for (int k = 0; k < matriz.length; k++) {
			System.out.println(Arrays.toString(matriz[k]));
		}
		System.out.println("Os valores da diagonal principal s�o:");
		for (int l = 0; l < 3; l++) {
			System.out.print(matriz[l][l] + " ");
		}
		sc.close();
	}

	/* 4) Fa�a um programa para criar e exibir a matriz identidade NxN. */
	public static void questao04() {

		Scanner sc = new Scanner(System.in);
		System.out.println("Digite a ordem da Matriz");
		int n = sc.nextInt();
		int[][] matriz = new int[n][n];

		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz.length; j++) {
				if (i == j) {
					matriz[i][j] = 1;
				} else {
					matriz[i][j] = 0;
				}

			}

		}
		for (int k = 0; k < matriz.length; k++) {
			System.out.println(Arrays.toString(matriz[k]));
		}
		sc.close();
	}

	/*
	 * 5) Ler uma matriz A de dimens�o N x N e verificar se a matriz � sim�trica.
	 * Uma matriz � sim�trica caso coincidir com a sua transposta, ou seja, se A =
	 * AT e a matriz transposta � o resultado da troca de linhas por colunas em uma
	 * determinada matriz. Ex.: Ver imagem no anexo.
	 */
	public static void questao05() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Digite a ordem da Matriz");
		int n = sc.nextInt();
		int[][] matriz = new int[n][n];
		int[][] matrizT = new int[n][n];

		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz.length; j++) {
				System.out.println("Digite os valores:");
				matriz[i][j] = sc.nextInt();
			}

		}
		for (int i = 0; i < matrizT.length; i++) {
			for (int j = 0; j < matrizT.length; j++) {
				matrizT[i][j] = matriz[j][i];
			}

		}
		for (int k = 0; k < matriz.length; k++) {
			System.out.println(Arrays.toString(matriz[k]));
		}
		System.out.println("------------------------------------");
		for (int k = 0; k < matriz.length; k++) {
			System.out.println(Arrays.toString(matrizT[k]));
		}
		System.out.println("----------------------------------");
		boolean ifSimetric = true;
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++) {
				if (matriz[i][j] != matriz[j][i]) {
					ifSimetric = false;
					break;
				}
			}

			if (!ifSimetric)
				break;
		}
		System.out.println(ifSimetric ? "� uma matriz sim�trica" : "N�o � uma matriz sim�trica");
	}

	/*
	 * 6) Um aluno possui 4 notas em cada uma das 5 disciplinas que cursa na
	 * faculdade. Fa�a um programa que leia as notas do aluno, e indicar a mais
	 * alta. Deve ser exibido tamb�m a matriz de notas deste aluno.
	 */
	public static void questao06() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Quantos alunos tem na turma: ");
		int numAlunos = sc.nextInt();
		double matrizNotas[][][] = new double[numAlunos][5][4];

		for (int i = 0; i < matrizNotas.length; i++) {
			System.out.println("Aluno " + (i + 1));
			for (int j = 0; j < matrizNotas[0].length; j++) {
				System.out.println("Disciplina: " + (j + 1));
				for (int k = 0; k < matrizNotas[0][0].length; k++) {
					System.out.print("Digite a " + (k + 1) + "� nota: ");
					matrizNotas[i][j][k] = sc.nextDouble();

				}
			}
		}

		System.out.println("\n=========");
		double maiorNotaTodos = Double.MIN_VALUE;
		String alunoDiciplina = "";
		for (int i = 0; i < matrizNotas.length; i++) {
			System.out.println("Aluno " + (i + 1));
			System.out.println("=========");
			double maiorNotaGeral = Double.MIN_VALUE;
			String disciplina = "";
			for (int j = 0; j < matrizNotas[0].length; j++) {
				System.out.println("Disciplina: " + (j + 1));
				System.out.println("Notas: " + Arrays.toString(matrizNotas[i][j]));
				double maior = Double.MIN_VALUE;
				for (int k = 0; k < matrizNotas[0][0].length; k++) {
					maior = matrizNotas[i][j][k] > maior ? matrizNotas[i][j][k] : maior;
				}
				System.out.println("Maior Nota: " + maior);
				System.out.println("");
				if (maiorNotaGeral < maior) {
					maiorNotaGeral = maior;
					disciplina = "Disciplina " + (j + 1);
				}
			}

			if (maiorNotaTodos < maiorNotaGeral) {
				maiorNotaTodos = maiorNotaGeral;
				alunoDiciplina = disciplina + " - Aluno " + (i + 1);
			}

			System.out.println("Maior nota em todas as disciplinas: " + maiorNotaGeral + " - " + disciplina);
			System.out.println("***********");
		}

		System.out.println("Maior nota entre todos os alunos: " + maiorNotaTodos + " - " + alunoDiciplina);
		System.out.println("***********");

		sc.close();
	}
}
