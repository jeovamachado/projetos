package entitties;

public class Empregados {

	private int id;
	private String nome;
	private double salary;
	
	public Empregados(int id, String nome, double d) {
		this.id = id;
		this.nome = nome;
		this.salary = d;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "id:" 
				+ id 
				+ ", nome:" 
				+ nome 
				+ ", salary:" 
				+ salary;
	}
	
}
