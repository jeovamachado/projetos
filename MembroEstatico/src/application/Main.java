package application;

import java.util.Scanner;

import util.Calculator;

/*Fa�a um programa para ler a cota��o do d�lar, e depois um valor em d�lares a ser comprado por
uma pessoa em reais. Informar quantos reais a pessoa vai pagar pelos d�lares, considerando ainda
que a pessoa ter� que pagar 6% de IOF sobre o valor em d�lar. Criar uma classe CurrencyConverter
para ser respons�vel pelos c�lculos.*/

public class Main {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("Qual a cota��o do Dolar Hoje?");
		double cotacao = sc.nextDouble();
		System.out.println("Quantos reais ser�o convertidos em D�lar ?");
		double valorReal = sc.nextDouble();
		System.out.println("O valor em D�lar �: ");
		System.out.printf(" $ %.2f%n ", Calculator.conversaoDolarReal(valorReal, cotacao));
	}

}
