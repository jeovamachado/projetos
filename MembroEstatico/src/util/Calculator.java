package util;

public class Calculator {
	
	
	public static final double IOF = 0.06;

	public static double conversaoDolarReal(double valorReal, double cotacao) {
		return ((valorReal * cotacao) + (IOF * valorReal * cotacao));

	}

}
