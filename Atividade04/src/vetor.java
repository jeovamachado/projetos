import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class vetor {

	private static Scanner leitor = new Scanner(System.in);

	public static void main(String[] args) {

		questao06();
		System.out.println();

	}

	public static void questao01() {
		/*
		 * 1) Faça um programa para gerar um vetor de 30 posições, onde cada elemento
		 * corresponde ao quadrado de sua posição. Mostre o vetor resultante.
		 */
		int vetor[];
		int i = 0;
		vetor = new int[30];
		for (i = 0; i < vetor.length; i++)
			vetor[i] = i * i;
		for (int index : vetor)
			System.out.println(index);
		System.out.println(Arrays.toString(vetor));
		System.out.println(vetor.length);

	}

	public static void questao02() {
		/*
		 * 2) Ler 15 valores reais em um vetor e imprimir os números localizados nas
		 * posições impares.
		 */

		float[] vetor = new float[15];
		for (int i = 0; i < vetor.length; i++) {
			System.out.println("Digite o valor da posição " + i);
			vetor[i] = leitor.nextFloat();
		}
		for (float index : vetor)
			if (index % 2 != 0) {
				System.out.println(index);
			}
	}

	/*
	 * 3) Desenvolver um programa para ler um vetor VAL de números inteiros e criar
	 * outro vetor VAL2 de mesma quantidade de elementos que VAL, onde os elementos
	 * tenham o dobro do valor dos elementos de VAL. O número máximo de elementos é
	 * 30.
	 */
	public static void questao03() {
		int n = 0;
		System.out.println("Digite o tamanho do vetor");
		n = leitor.nextInt();
		if (n > 0 && n < 30) {
			int vetor[] = new int[n];
			int vetor2[] = new int[n];
			for (int i = 0; i < vetor.length; i++) {
				System.out.println("Digite o valor da posição " + i);
				vetor[i] = leitor.nextInt();
				vetor2[i] = (vetor[i]) * 2;
			}
			System.out.println("Vetor1 " + Arrays.toString(vetor));
			System.out.println("Vetor2" + Arrays.toString(vetor2));

		} else {
			System.out.println("O valor do vetor não pode ser negativo e o tamanho máximo permitido é 30");

		}
	}

	/*
	 * 4) Escreva um algoritmo que recebe um inteiro 0 < n ≤ 100 e um vetor de n
	 * números inteiros cuja primeira posição é 1 e inverte a ordem dos elementos do
	 * vetor sem usar outro vetor.
	 */
	public static void questao04() {
		int n = 0;
		System.out.println("Digite o tamanho do vetor");
		n = leitor.nextInt();
		Integer vetor1[] = new Integer[n];
		if (n > 0 && n <= 100) {
			for (int i = 1; i < vetor1.length; i++) {
				vetor1[0] = 1;
				System.out.println("Digite os componentes do vetor na posição: " + i);
				vetor1[i] = leitor.nextInt();
			}
			Arrays.sort(vetor1, Collections.reverseOrder());
			System.out.println(Arrays.toString(vetor1));

		}
	}

	/*
	 * 5) Leia um vetor de 15 posições e reescreva-o de trás pra frente mostrando
	 * apenas as posições impares.
	 */
	public static void questao05() {
		Integer[] vetor = new Integer[15];
		for (int i = 0; i < vetor.length; i++) {
			System.out.println("Digite os componentes do vetor na posição: " + i);
			vetor[i] = leitor.nextInt();
		}

		Arrays.sort(vetor, Collections.reverseOrder());
		for (int i = 0; i < 15; i++) {
			if (vetor[i].intValue() % 2 != 0) {
				System.out.println(vetor[i]);
			}
		}

	}

	/*
	 * 6) Em uma sala com N alunos, faça um programa que informe a maior e a segunda
	 * maior nota.
	 */
	public static void questao06() {

		System.out.println("Digite a quantidade de alunos");
		int n = leitor.nextInt();
		float[] v = new float[n];
		for (int i = 0; i < n; i++) {
			System.out.println("Digite a nota do aluno " + i);
			v[i] = leitor.nextFloat();
		}
		Arrays.sort(v);
		//System.out.println(Arrays.toString(v));
		System.out.println("A maior nota é " + v[v.length-1] + " e a segunda maior é " + v[v.length-2]);

	}

}
