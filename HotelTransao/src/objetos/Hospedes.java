package objetos;


/** 10 Quartos
Estudantes[n]
vetQuarto[10]
Aluguel(Preco, Nome, email)
Obs. O aluno pode escolher o quarto
desde que o mesmo esteja vazio.
**/

public class Hospedes {
	
	
	private int numQuarto;
	private String nome;
	private double preco;
	private String mail;
	
	
	public int getNumQuarto() {
		return numQuarto;
	}
	public void setNumQuarto(int numQuarto) {
		this.numQuarto = numQuarto;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public double getPreco() {
		return preco;
	}
	public void setPreco(double preco) {
		this.preco = preco;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}

}
