package aplicacao;

import java.util.Scanner;

import objetos.Hospedes;

/**
 * 10 Quartos Estudantes[n] vetQuarto[10] Aluguel(Preco, Nome, email) Obs. O
 * aluno pode escolher o quarto desde que o mesmo esteja vazio.
 **/

public class Funcoes {

	private static Scanner leitor = new Scanner(System.in);

	public static Hospedes[] alugarQuarto(int qntE) {
		Hospedes[] vetorHotel = new Hospedes[10];

		Hospedes h = new Hospedes();
		for (int i = 0; i < 10; i++) {
			vetorHotel[i] = h;
			vetorHotel[i].setNumQuarto(10);
		}
		for (int i = 0; i < qntE; i++) {
			Hospedes hospede = hospedar(vetorHotel);
			vetorHotel[hospede.getNumQuarto()] = hospede;
		}
		return vetorHotel;
	}

	public static Hospedes hospedar(Hospedes[] vetorHotel) {
		/**
		 * numQuarto nome preco mail
		 */
		int numQuarto = 10;
		boolean aux = false;
		while (aux == false) {
			System.out.println("Insira o N� do quarto desejato:");
			numQuarto = leitor.nextInt();
			aux = checkQuarto(numQuarto, vetorHotel);
		}
		System.out.println("Nome do H�spede");
		String nome = leitor.next();
		System.out.println("Valor da hospedagem");
		Double preco = leitor.nextDouble();
		System.out.println("email");
		String mail = leitor.next();
		Hospedes hospede = new Hospedes();
		hospede.setNumQuarto(numQuarto);
		hospede.setNome(nome);
		hospede.setPreco(preco);
		hospede.setMail(mail);
		return hospede;
	}

	public static boolean checkQuarto(int numQuarto, Hospedes[] vetorHotel) {
		try {
			if (vetorHotel[numQuarto].getNumQuarto() == 10) {
				return true;
			} else {
				return false;
			}
		} catch (Exception E) {
			return false;
		}

	}

	public static Hospedes consulta(int numConsulta,Hospedes[] vetorHotel) {
		if (checkQuarto2(numConsulta, vetorHotel)) {
			return vetorHotel[numConsulta];
		} else {
			return null;
		}
	}
	public static boolean checkQuarto2(int numQuarto, Hospedes[] vetorHotel) {
		try {
			if (vetorHotel[numQuarto].getNumQuarto() != 10) {
				return true;
			} else {
				return false;
			}
		} catch (Exception E) {
			return false;
		}

	}

}
