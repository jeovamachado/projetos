package aplicacao;

import java.util.Scanner;

import objetos.Hospedes;

public class Principal {
	// As declara��es das vari�veis e instacia��o de objetos s�o feitos fora da
	// main.
	private static int quantEstudantes; // Vari�vel
	private static Scanner leitor = new Scanner(System.in); // Objeto da classe Scanner que foi importada onde
	private static int numConsulta;

	public static void main(String[] args) {
		AuxFunc();
	}

	/**
	 * 10 Quartos Estudantes[n] vetQuarto[10] Aluguel(Preco, Nome, email) Obs. O
	 * aluno pode escolher o quarto desde que o mesmo esteja vazio.
	 **/
	private static void AuxFunc() {
		System.out.println("Entre com a quantidade de Estudantes");
		quantEstudantes = leitor.nextInt();
		Hospedes[] valor = Funcoes.alugarQuarto(quantEstudantes);
		System.out.println("Digite o n�mero do quarto para consultar h�spede");
		numConsulta = leitor.nextInt();
		Hospedes hospede = Funcoes.consulta(numConsulta, valor);
		System.out.println("Nome " + hospede.getNome() + "\nEmail  " + hospede.getMail());
	}

}
