package entities;

/*Fazer um programa para ler o nome de um aluno e as tr�s notas que ele obteve nos tr�s trimestres do ano
(primeiro trimestre vale 30 e o segundo e terceiro valem 35 cada). Ao final, mostrar qual a nota final do aluno no
ano. Dizer tamb�m se o aluno est� aprovado (PASS) ou n�o (FAILED) e, em caso negativo, quantos pontos faltam
para o aluno obter o m�nimo para ser aprovado (que � 60% da nota). Voc� deve criar uma classe Student para
resolver este problema.*/


public class Aluno {
	private String nome;
	private double nota1;
	private double nota2;
	private double nota3;
	private double notaFinal;
	
	
	public Aluno(String nome, double nota1, double nota2, double nota3) {
		super();
		this.nome = nome;
		this.nota1 = nota1;
		this.nota2 = nota2;
		this.nota3 = nota3;
		notaFinal();
	}

	@Override
	public String toString() {
		return "aluno nome=" + nome + ", nota1=" + nota1 + ", nota2=" + nota2 + ", nota3=" + nota3 + ", notaFinal="
				+ notaFinal;
	}
	
	public void notaFinal() {
		notaFinal = nota1 +nota2+nota3;
	}
	public void mediaTrimestral() {
		if(notaFinal < 60.00) {
			System.out.println("N�o Passou");
			System.out.printf("Faltam %.2f pontos%n" , (60 -notaFinal));
		}else {
			System.out.println("Passou");
		}
			
	}
	
}
