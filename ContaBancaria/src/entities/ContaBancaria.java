package entities;

/*Importante: uma vez que uma conta banc�ria foi aberta, o n�mero da conta nunca poder� ser alterado. J�
o nome do titular pode ser alterado (pois uma pessoa pode mudar de nome por ocasi�o de casamento, por
exemplo).
Por fim, o saldo da conta n�o pode ser alterado livremente. � preciso haver um mecanismo para proteger
isso. O saldo s� aumenta por meio de dep�sitos, e s� diminui por meio de saques. Para cada saque
realizado, o banco cobra uma taxa de $ 5.00. Nota: a conta pode ficar com saldo negativo se o saldo n�o for
suficiente para realizar o saque e/ou pagar a taxa.*/
public class ContaBancaria {

	private int idConta;
	private String titular;
	private double saldo;

	public ContaBancaria(int idConta, String titular) {
		this.idConta = idConta;
		this.titular = titular;
		saldo = 0;
	}

	public ContaBancaria(int idConta, String titular, double depositoInicial) {
		this.idConta = idConta;
		this.titular = titular;
		saldo = depositoInicial;
	}

	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public int getIdConta() {
		return idConta;
	}

	public double getSaldo() {
		return saldo;
	}

	@Override
	public String toString() {
		return "ContaBancaria idConta=" + idConta + ", titular=" + titular + ", saldo=" + saldo;
	}

	public void deposito(double entrada) {
		saldo= saldo + entrada ;
	}

	public void saque(double saida) {
		saldo = saldo - saida - 5.00;
	}
}