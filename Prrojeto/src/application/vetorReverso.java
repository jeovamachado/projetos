package application;

import java.util.Arrays;
import java.util.Collections;

public class vetorReverso {

	public static void main(String[] args) {
		Integer[] cubes = new Integer[] { 8, 27, 64, 125, 256 }; 
		Arrays.sort(cubes, Collections.reverseOrder());
		System.out.println(Arrays.deepToString(cubes));
		
		
	}

}
