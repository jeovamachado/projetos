package application;

import java.util.Scanner;

public class primeNumber {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int primeNumber;
		int primeDividers = 0;
		System.out.println("Enter a number");
		primeNumber = reader.nextInt();
		
		for (int i = 1; i <= primeNumber; i++) {
			if (primeNumber % i == 0) {
				primeDividers++;
			}

		}
		if (primeDividers == 2) { 
			System.out.println("The number entered is prime");
		} else {
			System.out.println("The number entered is not prime");
		}
	}
}
