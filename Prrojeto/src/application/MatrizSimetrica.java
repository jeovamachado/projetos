package application;

import java.util.Scanner;

public class MatrizSimetrica {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		questao05();
		System.out.println();
	}

	public static void questao05() {
		System.out.println("Criando Matriz NxN: ");
		System.out.print("Digite o valor de N: ");
		int l = sc.nextInt();

		// int l = random.nextInt(4);
		// System.out.println(l);

		int matriz[][] = new int[l][l];
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++) {
				System.out.print("Digite o valor posicao [" + i + ", " + j + "]: ");
				matriz[i][j] = sc.nextInt();

				// matriz[i][j] = random.nextInt(10);
				// System.out.println(matriz[i][j]);
			}
		}

		// Matriz Original || Simetrica
		for (int i = 0; i < matriz.length; i++) {
			System.out.println();
			for (int j = 0; j < matriz[0].length; j++) {
				System.out.printf("%5d ", matriz[i][j]);
			}
			System.out.print("\t| |");
			for (int j = 0; j < matriz[0].length; j++) {
				System.out.printf("%5d ", matriz[j][i]);
			}
			System.out.println();
		}

		boolean ehSimetrica = true;
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++) {
				if (matriz[i][j] != matriz[j][i]) {
					ehSimetrica = false;
					break;
				}
			}

			if (!ehSimetrica)
				break;
		}

		System.out.println(ehSimetrica ? "� uma matriz sim�trica" : "N�o � uma matriz sim�trica");
	}

}
