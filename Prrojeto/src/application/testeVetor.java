package application;

public class testeVetor {

	public static void main(String[] args) {
		int vetor[]; /* Declara��o do tipo de varialvel */
		vetor = new int[10]; /* Declarando um vetor espec�fico de 10 posi��es */
		for (int i = 0; i <vetor.length; i++)
			vetor[i] = 1 + 2 * i;

		for (int index : vetor) /* La�o espec�fico para percorrer todos os valores de um vetor v[n]*/
			System.out.println(index); /* Aqui ela vai mostrar os valores alocados nas posi��es do vetor por esse apelido*/
	}

}
