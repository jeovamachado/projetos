package application;

import java.util.Arrays;
import java.util.Scanner;


public class testeVetor_02 {

	public static void main(String[] args) {
		testeVetor();
	}

	public static void testeVetor() {
		Scanner leitor = new Scanner(System.in);
		System.out.println("Digite o tamanho do vetor");
		int n = leitor.nextInt();
		double[] vet = new double[n];
		System.out.println("O tamanho do vetor �:" + vet.length);
		for (int i = 0; i < n; i++) {
			System.out.println("Digite os valores que ser�o armazenados");
			vet[i] = leitor.nextDouble();	
		}
		System.out.println(Arrays.toString(vet));
		Arrays.sort(vet);
		System.out.println(Arrays.toString(vet));
		leitor.close();
	}
}
