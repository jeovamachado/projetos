package entities;

public class Product {
	public String name;		/*atributos da classe*/
	public double price;
	public int quantity;
	
	public Product(String name, double price, int quantity) {
		this.name = name;	/*par�metros do m�todo*/
		this.price = price;
		this.quantity = quantity;
	}
	public Product(String name, double price) {
		this.name = name;	/*par�metros do m�todo*/
		this.price = price;
	
	}

	public double totalValueInStock() {
		return price * quantity;
	}

	public void addProducts(int quantity) {
		this.quantity += quantity;
	}

	public void removeProducts(int quantity) {
		this.quantity -= quantity;
	}

	public String toString() {
		return name + ", $ " + String.format("%.2f", price) + ", " + quantity + " units, Total: $ "
				+ String.format("%.2f", totalValueInStock());
	}
}