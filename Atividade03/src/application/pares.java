package application;

import java.util.Scanner;

public class pares {

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		questao01();
		System.out.println();
		questao02();
		System.out.println();
		questao03();
		System.out.println();
		questao04();
		System.out.println();
		questao05();
		System.out.println();
		questao06();
		System.out.println();
		questao07();
		System.out.println();
		questao08();
		System.out.println();
	}

	public static void questao01() {
		// 1) Imprimir os n�meros pares de 10 a 100.
		int num = 10;
		for (int i = 1; i <= 46; i++) {
			if (num % 2 == 0)
				System.out.println(num);
			num = num + 2;
		}
	}

	public static void questao02() {
		/* 2) Imprimir a sequ�ncia: 11, 21, 31... 101. */
		int num = 1;
		for (int i = 0; i <= 9; i++) {
			num = num + 10;
			System.out.println(num);
		}
	}

	public static void questao03() {
		/* 3) Ler 10 n�meros reais e imprimir o maior. */
		int maior = 0;
		int n;
		for (int i = 0; i < 10; i++) {
			System.out.println("Digite um n�mero real");
			n = sc.nextInt();
			if (n > maior)
				maior = n;
		}
		System.out.println(" O maior �:" + maior);
		sc.close();

	}

	public static void questao04() {
		// 4) Ler 10 n�meros e exibir a soma dos impares.
		int y;
		int soma = 0;
		for (int i = 0; i < 10; i++) {
			System.out.println("Digite um n�mero N" + (i + 1));
			y = sc.nextInt();
			if (y % 2 != 0) {
				soma = soma + y;
			}

		}
		System.out.println(soma);

	}

	public static void questao05() {
		// 5) Ler 10 n�meros e exibir a soma dos m�ltiplos de 5.
		int y;
		int soma = 0;
		for (int i = 0; i < 10; i++) {
			System.out.println("Digite um n�mero N" + (i + 1));
			y = sc.nextInt();
			if (y % 5 == 0) {
				soma = soma + y;
			}

		}
		System.out.println(soma);

	}

	public static void questao06() {
		// 6) Ler diversos n�meros e exibir quantos foram digitados. O valor -1 ser� a
		// condi��o de parada.
		int x = 0;
		int quant = 0;
		while (x != -1) {
			System.out.println("Digite um n�mero (Obs. -1 � crit�rio de parada)");
			x = sc.nextInt();
			if (x != -1)
				quant++;

		}
		System.out.println("A quantidade de valores digitados � :" + quant);
	}

	/*
	 * 7) Ler um somat�rio de n�meros inteiros que ir� parar ao ser digitado o
	 * n�mero 0 e calcular a m�dia destes n�meros digitados.
	 */
	public static void questao07() {
		int x = 1;
		int quant = 0;
		int aux = 0;
		float media = 0;
		while (x != 0) {
			System.out.println("Digite um n�mero (Obs. 0 � crit�rio de parada)");
			x = sc.nextInt();
			aux = aux + x;
			if (x != 0)
				quant++;

		}
		media = aux / quant;
		System.out.println("A m�dia � : " + media);
	}

	/*
	 * 8) Leia o genero e a altura de v�rias pessoas (parando ao encontrar uma
	 * altura zero), calcule a m�dia das alturas dos homens e das mulheres, escrever
	 * a quantidade de homens e mulheres e qual o grupo � o mais alto.
	 */
	public static void questao08() {
		float altura = 1;
		char genero;
		int homi = 0;
		int muie = 0;
		float media = 0;
		float soma = 0;
		float soma2 = 0;
		while (altura != 0) {
			System.out.println("Digite o g�nero");
			genero = sc.next().charAt(0);
			System.out.println("Digite a altura");
			altura = sc.nextFloat();
			if (genero == 'M' && altura != 0) {
				homi++;
				soma = altura + soma;
			} else if (genero == 'F' && altura != 0) {
				muie++;
				soma2 = altura + soma2;
			}
		}
		System.out.println("A quantidade de Homens � " + homi + "\nAquantidade de Mulheres � " + muie);
		System.out.println("A soma das alturas dos Homens " + soma + "\nA soma das alturas das Mulheres � " + soma2);
		System.out.println("A m�dia das alturas dos Homens " + soma / homi + "\nA m�dia das alturas das Mulheres � "
				+ soma2 / muie);
		if (soma / homi > soma2 / muie) {
			System.out.println("O grupo mais alto � o Masculino");
		} else {
			System.out.println("O grupo mais alto � o Feminino");
		}

	}
}