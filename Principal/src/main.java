import java.util.Arrays;
import java.util.Scanner;

public class main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int[] v = new int[3];
		bolha(v);

		for (int i = 0; i < 3; i++) {
			System.out.println("Digite os valores");
			v[i] = sc.nextInt();

		}
		System.out.println("A lista dos 3 n�meros digitados:");
		System.out.println(Arrays.toString(v));
		sc.close();
		bolha(v);
		System.out.println("Os n�meros ordenados:");
		System.out.println(Arrays.toString(v));
		int valorMax = v[v.length - 1];
		System.out.println(valorMax);
	}

	private static void bolha(int[] v) {

		for (int last = v.length - 1; last > 0; last--) {
			for (int i = 0; i < last; i++) {
				if (v[i] > v[i + 1])
					trocar(v, i, i + 1);
			}
		}
	}

	private static void trocar(int[] v, int i, int j) {
		int aux = v[i];
		v[i] = v[j];
		v[j] = aux;
	}

}