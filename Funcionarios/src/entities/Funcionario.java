package entities;

/*Fazer um programa para ler os dados de um funcion�rio (nome, sal�rio bruto e imposto). Em
seguida, mostrar os dados do funcion�rio (nome e sal�rio l�quido). Em seguida, aumentar o
sal�rio do funcion�rio com base em uma porcentagem dada (somente o sal�rio bruto �
afetado pela porcentagem) e mostrar novamente os dados do funcion�rio. Use a classe
projetada abaixo.*/



public class Funcionario {
	private String nome;
	private double salarioBruto;
	private double salarioLiquido;
	private double imposto;
	private float aumentoPercentual=0;
	
	
	public Funcionario(String nome, double salarioBruto, double imposto) {
		this.nome = nome;
		this.salarioBruto = salarioBruto;
		this.imposto = imposto;
		setSalarioLiquido();
		
		
	}


	public void setAumentoPercentual(float aumentoPercentual) {
		this.aumentoPercentual = aumentoPercentual;
	}


	public void setSalarioLiquido() {
		salarioLiquido = salarioBruto - imposto;
		
	}


	@Override
	public String toString() {
		return "Funcionario [nome=" + nome + ", salarioBruto=" + salarioBruto + ", salarioLiquido=" + salarioLiquido
				+ ", imposto=" + imposto + ", aumentoPercentual=" + aumentoPercentual + "]";
	}
	public void aumentoRecebido() {
		salarioBruto = salarioBruto + (aumentoPercentual * salarioBruto)/100;
		setSalarioLiquido();
	}
	
}
