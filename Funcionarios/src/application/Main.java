package application;

import java.util.Scanner;

import entities.Funcionario;

public class Main {
	
/*Name: Joao Silva
Gross salary: 6000.00
Tax: 1000.00
Employee: Joao Silva, $ 5000.00
Which percentage to increase salary? 10.0
Updated data: Joao Silva, $ 5600.00*/

	public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);
	System.out.println("Digite o nome");
	String nome= sc.nextLine(); 
	System.out.println("Digite o Sal�rio Bruto");
	double salarioBruto= sc.nextDouble();
	System.out.println("Digite o imposto");
	double imposto = sc.nextDouble();
	Funcionario funcionario = new Funcionario(nome,salarioBruto,imposto);
	System.out.println(funcionario);
	
	System.out.println("Digite o aumento percentual");
	funcionario.setAumentoPercentual(sc.nextFloat());
	funcionario.aumentoRecebido();
	System.out.println(funcionario);

	}

}
