package entities;


/*1) A sua equipe de programadores est� trabalhando no desenvolvimento de um sistema para o gerenciamento 
 * de livros de uma biblioteca. A sua tarefa � implementar um TAD para representar os livros neste sistema.
 *  Sabe-se que um livro � representado pelo seguinte tipo estruturado:
*livro {
*String titulo;
*String autor;
*String genero;
*int ano;
}*/
public class Books {

	private String title;
	private String autor;
	private String gender;
	private int year;

	public Books() {

	}

	public Books(String title, String autor, String gender, int year) {
		this.title = title;
		this.autor = autor;
		this.gender = gender;
		this.year = year;
	}
	/*
	 * Os m�todos que devem ser descritos pelo TAD Livro (na interface do TAD), s�o
	 * as seguintes: O m�todo construtor que recebe por par�metro o t�tulo, autor,
	 * g�nero e ano de publica��o do livro, cria um livro com esses dados e retorna
	 * um objeto para o novo Livro. Quatro m�todos de obten��o dos dados armazenados
	 * em um TAD Livro denominados: � getGenero; � getAutor; � getTitulo; � getAno.
	 */

	public String getTitle() {
		return title;
	}

	public String getAutor() {
		return autor;
	}

	public String getGender() {
		return gender;
	}

	public int getYear() {
		return year;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setYear(int year) {
		this.year = year;
	}

	@Override
	public String toString() {
		return "Title of book:" + title + ", Autor:" + autor + ", Gender:" + gender + ", Year:" + year;
	}

}
