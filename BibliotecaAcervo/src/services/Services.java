package services;

import java.util.Scanner;

import entities.Books;

public class Services {

	public static Scanner sc = new Scanner(System.in);

	public static Books[] inputOnUserDef() {

		String title;
		String autor;
		String gender;
		int year;
		System.out.println("How many books will be added ?");
		int n = sc.nextInt();
		sc.nextLine();
		Books[] vect = new Books[n];

		for (int i = 0; i < n; i++) {
			System.out.println("Enter a book Title:");
			title = sc.nextLine();
			System.out.println("Enter a book Autor:");
			autor = sc.nextLine();
			System.out.println("Enter a book Gender:");
			gender = sc.nextLine();
			System.out.println("Enter a book publication year:");
			year = sc.nextInt();
			System.out.println("*******************************************");
			sc.nextLine();
			vect[i] = new Books(title, autor, gender, year);
		}

		for (int k = 0; k < vect.length; k++) {
			System.out.println(vect[k]);
		}

		return vect;
	}

	/*
	 * Um sexto m�todo est�tico, denominado verificaNoModernismo que recebe um
	 * objeto de Livro e verifica se esse livro pertence ao segundo per�odo do
	 * modernismo brasileiro (1930 a 1945). Este m�tiodo retorna -1 se o ano da obra
	 * for anterior a 1930, retorna 0 se for no per�odo 1930 a 1945, e retorna 1 seo
	 * ano for posterior a 1945.
	 * 
	 */

	public static int verifyInModernism(Books book) {
		if (book.getYear() < 1930) {
			return -1;
		} else if (book.getYear() >= 1930 && book.getYear() <= 1945) {
			return 0;

		} else {
			return 1;
		}
	}

	public static void searchBook(Books[] book) {

		int y = 1;
		System.out.println("Seach Autor on library:");
		sc.hasNextLine();
		String x = sc.nextLine();
		for (int i = 0; i < book.length; i++) {
			if (book[i].getAutor().equalsIgnoreCase(x)) {
				y = 2;
				System.out.println(" Result for your search: " + book[i].getTitle());
			}
		}
		if (y == 1) {
			System.out.println("The book was not found");
		}
	}

	public static Books[] autoFillBooks() {
		
		int n = 4;
		Books[] vect = new Books[n];
		vect[0] = new Books("Novos Poemas", "Vinicius de Morais", "poesia", 1938);
		vect[1] = new Books("Poemas Escritos na India", "Cecilia Meireles", "poesia", 1962);
		vect[2] = new Books("Orfeu da Concei��o", "Vinicius de Morais", "teatro", 1954);
		vect[3] = new Books("Ariana, a Mulher", "Vinicius de Morais", "poesia", 1936);
		for (int k = 0; k < vect.length; k++) {
			System.out.println(vect[k]);
		}

		return vect;
	}

	public static Books[] orderBooks(Books[] vect) {
		System.out.println("Para ordenar os livros: \n Por T�tulo digite: 1, por autor: 2, por g�nero: 3 e por ano: 4");
		int key = sc.nextInt();
		Books livro;
		switch (key) {
		case 1:
			for (int i = 0; i < vect.length; i++) {
				for (int j = i + 1; j < vect.length; j++) {
					if (vect[i].getTitle().compareTo(vect[j].getTitle()) > 0) {
						livro = vect[i];
						vect[i] = vect[j];
						vect[j] = livro;
					}
				}

			}
			break;
		case 2:
			for (int i = 0; i < vect.length; i++) {
				for (int j = i + 1; j < vect.length; j++) {
					if (vect[i].getAutor().compareTo(vect[j].getAutor()) > 0) {
						livro = vect[i];
						vect[i] = vect[j];
						vect[j] = livro;
					}
				}

			}

			break;
		case 3:
			for (int i = 0; i < vect.length; i++) {
				for (int j = i + 1; j < vect.length; j++) {
					if (vect[i].getGender().compareTo(vect[j].getGender()) > 0) {
						livro = vect[i];
						vect[i] = vect[j];
						vect[j] = livro;
					}
				}
				
			}
			break;
		case 4:
			for (int i = 0; i < vect.length; i++) {
				for (int j = i + 1; j < vect.length; j++) {
					if (vect[i].getYear() > vect[j].getYear()) {
						livro = vect[i];
						vect[i] = vect[j];
						vect[j] = livro;
					}
				}
				
			}
			break;

		}

		for (int k = 0; k < vect.length; k++) {
			System.out.println(vect[k]);
		}
		return vect;
	}

}
