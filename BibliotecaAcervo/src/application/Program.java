package application;

import entities.Books;
import services.Services;

public class Program {

	/*
	 * 2) Escreva o conte�do do arquivo Livro.java com a interface deste TAD Livro,
	 * incluindo a defini��o da estrutura e tamb�m todos os m�todos dispon�veis para
	 * o usu�rio. Em seguida, escreve uma classe principal de um programa
	 * (Principal.java) que crie e inicialize um vetor de objetos para Livro
	 * utilizando os m�todos disponibilizadas pelo TAD e inserindo neste vetor os
	 * seguintes livros: "Novos Poemas", "Vinicius de Morais", "poesia", 1938
	 * "Poemas Escritos na India", "Cecilia Meireles", "poesia", 1962
	 * "Orfeu da Concei��o", "Vinicius de Morais", "teatro", 1954
	 * "Ariana, a Mulher", "Vinicius de Morais", "poesia", 1936
	 */
	public static void main(String[] args) {
		Books[] book = Services.autoFillBooks();
		System.out.println("**********************************");
		// book = Services.inputOnUserDef();
		// System.out.println("**********************************");
		Services.searchBook(book);
		System.out.println("**********************************");
		System.out.println(Services.verifyInModernism(book[1]));
		System.out.println("**********************************");
		Services.orderBooks(book);
		System.out.println("**********************************");
		Services.sc.close();
	}

}
