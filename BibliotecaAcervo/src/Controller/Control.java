package Controller;



public interface Control {
     public void inputOnUserDef();
     public void verifyInModernism();
     public void searchBook();
}
