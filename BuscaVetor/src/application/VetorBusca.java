package application;

import java.util.Scanner;

public class VetorBusca {
	
	static boolean busca( String x, String [] vetor) {
		for(int i = 0; i < vetor.length; i++) {
			if(vetor[i] == x) {
				return true;
			}
		}
		return false;
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String [] vetor = new String[4];
		int i =0;
		while (i<4) {
			System.out.println("Digite o " + (i +1) +"� nome: ");
			vetor[i] = sc.nextLine();
			i++;
		}
		System.out.println("Digite o elemento de busca");
		String x = sc.nextLine();
		boolean resultado = busca(x, vetor);
		if (resultado == true) {
			System.out.println("O nome foi encontrado");
		}
		System.out.println("O nome n�o foi encontrado");
		sc.close();
	}

}
