package estudo_de_listas;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class listas {

	public static void main(String[] args) {
		
		List<String> list = new ArrayList<>();  // A lista diferente do vetor, n�o aceita os tipos primitivos (int)
		
		list.add("Maria");
		list.add("Jo�o");
		list.add("Pedro");
		list.add("Salvador");
		list.add("Jessica");
		list.add(2, "Marcos");
		
		//list.remove("Anna");
		//list.remove(1);
		System.out.println(list.size());
		for(String x: list) {
			System.out.println(x);
		}
		System.out.println("---------------------------------------");
		list.removeIf( x -> x.charAt(0) == 'M'); // remove os itens da lista que possuem M na posi��o 0 da string
		for(String x: list) {
			System.out.println(x);		
		}
		System.out.println("Index Of Bob: " +list.indexOf("Salvador"));
		System.out.println("---------------------------------------");
		List<String> result = list.stream().filter(x -> x.charAt(0) == 'J').collect(Collectors.toList());
		for(String x: result) {
			System.out.println(x);		
		}
		System.out.println("---------------------------------------");
		String name = list.stream().filter(x -> x.charAt(0) == 'J').findFirst().orElse(null);
		System.out.println(name);
		String name2 = list.stream().filter(x -> x.charAt(0) == 'J').findAny().orElse(null);
		System.out.println(name2);
	}

	
}
